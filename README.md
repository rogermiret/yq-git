# YQ - Git

This is a helper image that contains `yq` and `git`. My intention is to use it on a gitops pipeline, but couldn't find an appropriate image.

## Build

```shell
# "Normal" build
docker build -t yq-git .
# Building for a specific platform
docker buildx build -t yq-git --platform=linux/arm64 .
```
